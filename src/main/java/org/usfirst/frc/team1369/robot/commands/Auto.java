package org.usfirst.frc.team1369.robot.commands;

import org.usfirst.frc.team1369.robot.Utils;
import org.usfirst.frc.team1369.robot.subsystems.DriveTrain;
import org.usfirst.frc.team1369.robot.Robot;

public abstract class Auto {

	public DriveTrain driveTrain;
	public Thread thread;
	public Object threadLock;
	public 	int delay;
	
	public Auto() {
		this.driveTrain = Robot.driveTrain;
	}

	public void start(){
		Utils.sleep(delay);
	}
	
	public abstract void auto();
	public abstract void loop();
	public abstract void stop();
	
}