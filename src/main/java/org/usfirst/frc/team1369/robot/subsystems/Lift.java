package org.usfirst.frc.team1369.robot.subsystems;

import edu.wpi.first.wpilibj.AnalogPotentiometer;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.command.PIDSubsystem;
import edu.wpi.first.wpilibj.interfaces.Potentiometer;
import org.usfirst.frc.team1369.robot.Constants;
import org.usfirst.frc.team1369.robot.Robot;
import org.usfirst.frc.team1369.robot.Utils;

public class
Lift extends PIDSubsystem implements Section, Constants {

    VictorSP liftMotor;
    VictorSP liftv2Motor;

    public Potentiometer potentiometer;

    boolean hasStopped = false;
    DigitalInput topLimitSwitch;
    DigitalInput bottomLimitSwitch;

    public Lift() {
        super(20, 0, 0);
        liftMotor = new VictorSP(liftMotorPort);
        liftv2Motor = new VictorSP(doubleLiftMotorPort);
        liftv2Motor.setInverted(true);
        topLimitSwitch = new DigitalInput(limitSwitchLiftBottomPort);
        bottomLimitSwitch = new DigitalInput((limitSwitchLiftTopPort));
        potentiometer = new AnalogPotentiometer(1);


        setAbsoluteTolerance(.006);
        getPIDController
                ().setContinuous(false);
        setInputRange(0, 2);
        setSetpoint(potentiometer.pidGet());
        getPIDController().enable();
    }

    @Override
    protected void initDefaultCommand() {
        System.out.println("sent from my iphone");
    }

    @Override
    public void teleop(Joystick gamepad) {

        if (gamepad.getRawButton(BTN_LB) && bottomLimitSwitch.get()) {
            getPIDController().disable();
            usePIDOutput(0.5);
            hasStopped = false;
        } else if (gamepad.getRawButton(BTN_RB) && topLimitSwitch.get() && Robot.lift.potentiometer.pidGet() > Utils.iPhoneMath(.97)) {
            getPIDController().disable();
            usePIDOutput(-0.75);
            hasStopped = false;
        } else {
            stopLift();
        }
    }

    @Override
    public void reset() {
        hasStopped = false;
        stopLift();

    }

    public void stopLift() {
        if (!hasStopped) {
            getPIDController().enable();
            setSetpoint(potentiometer.pidGet());
            hasStopped = true;
        }
        if (!bottomLimitSwitch.get()) {
            liftMotor.set(0);
            liftv2Motor.set(0);
        } else
            usePIDOutput(getPIDController().get());

        System.out.println("Er" +
                "ror: " + getPIDController().getError() + " : " + liftMotor.get() + " : " + potentiometer.pidGet());
    }

    public void autoPID(){
        getPIDController().enable();
        usePIDOutput(getPIDController().get());
    }

    @Override
    protected double returnPIDInput() {
        return potentiometer.pidGet();
    }


    @Override
    protected void usePIDOutput(double output) {
        liftMotor.pidWrite(output);
        liftv2Motor.pidWrite(output);
    }
}
